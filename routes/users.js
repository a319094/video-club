const express = require('express');
const router = express.Router();
const controller = require('../controllers/users');

/* GET users listing. => /users/ */
router.get('/', controller.userHome);

module.exports = router;
